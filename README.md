# Semi-automatic data processing and neuron detection

![](assets/labeling_screenshot.png)

## Authors
Erwan Grandgirard (grandgie@igbmc.fr)

Marco Dalla Vecchia (dallavem@igbmc.fr)

## Environment
Macros were developed and test in Fiji (ijm macro language).

## Purpose
This set of macro were created to assist users to manipulate and semi-automatically analyze death of live seeded growing neurons in large datasets (24-wells, multiples tiles and several days).

Althought this was created with a specific intent and application, hopefully with little modifications this could be useful for other users looking for a user-assisted analysis pipeline.

## Pipeline Description
### 01 Data pre-processing
In our user-case we had many multi-tiles multi-wells .czi files for each day of measurements. A single file would, for example, contain > 20 tiles for 6 wells (1 row of plate). In order to have all the time series information available in a single file, we want to concatenate all times points for the same wells together. This however creates very large files which can tricky to handle, especially if this is done on a personal laptop.

In order to solve this problem, we proceeded to split each individual well for each time point separately before concatenating them together. This was done using *batch* mode _Split Scenes (write files)_ in ZEN blue. *To make this work make sure no spaces or special characters are included in the file names!*

### 02 Data concatenation
Individual wells for each day are concatenated using the 02_concatenate_images.ijm macro. This macro reads the last part of the file names to identify the well names and concatenate all files of the same well in alphabetical order.

Make sure all files have the same name structure (names are split by the "-" symbol) and that they are consistently names in order for the concatenation to happen in the right order.

### 03 Neurons detection
Neurons are detected through a threshold and particle analysis approach. The first frame of the stack is set to a standard contrast and an automatic threshold is used to obtain a binary mask. Analyze Particles is used with user-defined minimum area (to remove background small specks or cell fragments) and maximum circularity (to remove dying neurons at first frame) to define detected neurons. These are shuffled and saved to disk. Individual crops of detected neurons are individually saved up to the maximum number of neurons defined by the user. Neuron crops are now available for semi-automatic labeling by the expert.

### 04 Neurons labeling
Individual neuron crops are now semi-automatically labeled by an expert user. We display all time points as a horizontal montage together with the additional options to label each crop as bad, out of frame or still alive. A user simply selects the folder containing all the crops and will be prompted to click on the corresponding frame in which the neuron died (or if it's a bad segmentation, if it went out of frame or if the neuron is still alive). For each labeled neuron we save in the Results table the corresponding clicked frame of death (or a specific values associated with the described conditions). This is done using 04_neuron_labeling.ijm macro.
