//Init
run("Clear Results");
run ("Close All");
if (isOpen("Log")) {
	selectWindow("Log");
	run("Close" );
}
// Choose directory that contains the choice.tif file
choiseDirectory=getDirectory("Choose the folder with choise image");

// Choose folder that contains individual neuron crops generated from previous macro
mySaveCropFolder=getDirectory("Select the folder with crops");
fileList = getFileList(mySaveCropFolder); 

// Sometimes it's useful to analyze only a subset of detected neurons
numberOfNeurons= getNumber("How many neurons would you like to analyse ?", 5);

// Initialize counter for neuron counting
counter = 0;

// Change measurements settings to only store label and X/Y of mouse cursor
run("Set Measurements...", "display redirect=None decimal=3");

print("LEGEND:\n6666 -> BAD\n9999 -> OUT OF FRAME\n1000 -> STILL ALIVE");

// Start looping through all the files up to number of specified neurons
for (f=0; f<fileList.length && counter < numberOfNeurons; f++){
		// Open image
		open(mySaveCropFolder + fileList[f]);

		OrigineName=getTitle();
		rename("timestack");
		// Check how many times points there are
		nslide=nSlices();
		// Open the choice image
		open(choiseDirectory+"Choice.tif");
		rename("Choice");	
		// Append the Choice image behind the neuron crop stack to make montage
		run("Concatenate...", "open image1=timestack image2=Choice");
		// Make montage to visualize all neuron frames plus the 3 choice icons
    	run("Make Montage...", "columns="+ (nslide +3) +" rows=1 scale=1 first=0");
    	rename(OrigineName);
    	title=getTitle();
    	
    	// Adjust contrast to avoid to have to manually set it for every new opened image

    	setMinAndMax(0, 1000);

		run("Scale to Fit");
		// Change to Point Tool so we can detect mouse coordinates
		setTool("point");
		run("Point Tool...", "type=Hybrid color=Pink size=[Extra Large] add");
	
		//wait a mouse clic!
		n = roiManager('count');
		while (n==0) {
			n = roiManager('count');
			wait(100);
		}
	
		// Extract X coordinate (we don't really care about Y)
		roiManager("measure");
		X=getResult("X", f);	

		// Since we know that each neuron crop has 150x150 size we can check which frame we are clicking on by dividing the cursor X position by 150
		// Change this to a variable if it becomes relevant
		frame = Math.ceil(X/150);	
//		print(frame);
//		print(nslide);
		
		// If the corresponding clicked frame is within the neuron time stack
		// append the frame number to the result table
		if (frame <= nslide) {
			setResult("Frame", f, frame);
			updateResults();
			print("Neuron marked dead at frame " + frame);		
		}
		// otherwise check if the mouse was clicked on any of the 3 icons
		else {
			// if it's the BAD icon
			if (frame == nslide +1) {
				setResult("Frame", f, "6666");
				updateResults();
				print("Neuron marked as BAD");		
			}
			// if it's the OUT OF FRAME icon
			if (frame == nslide +2) {
				setResult("Frame", f, "9999");
				updateResults();
				print("Neuron marked as OUT OF FRAME");	
			}
			// if it's the HEART icon
			if (frame == nslide +3) {
				setResult("Frame", f, "1000");
				updateResults();
				print("Neuron marked as STILL ALIVE");		
			}
		}
		// increase counter for neuron number
		// right now we are counting every neuron, move this into corresponding if statement if you only want to count neurons that died
		counter++;
		print("counter = " + counter);
		
		// Clean up stuff				
		roiManager("delete");
		selectWindow(title);
		close();
		selectWindow("Untitled");
		close();
//		selectWindow("timestack");
//		close();
		//counter=nResults;
		//print(counter);
}
// Change back to rectangle Tool not to have the Point Tool at the end
setTool("rectangle");
//break




