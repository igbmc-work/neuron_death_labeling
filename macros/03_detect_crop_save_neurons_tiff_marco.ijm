//Init
run("Clear Results");
print("\\Clear");
run("Fresh Start");

//enable macro functions for Bio-formats Plugin
//run("Bio-Formats Macro Extensions");	

// Select folder to import
print("select folder with your Data")
Directory = getDirectory("Choose a Directory");
// Select destination folder
print("Select the Save Folder")
mySaveCropFolder = getDirectory("Choose a Directory");
maxNeurons= getNumber("How many neuron would like to detect", 20);

min_area = getNumber("Set Minimum Area:", 200);
max_circ = getNumber("Set Max Circularity:", 0.8);
// Get list of files to process
list = getFileList(Directory);

// Ask how many neurons you want to analyze
// Could be left at *all available neurons* but we preferred to limit number
//nbNeuron= getNumber("How many neuron would like to detect", 20);

setBatchMode(false);

// PROCESS FILES
for (i = 0; i <list.length ; i++) {
	//print(Directory+list[i]);
	OpenFile(Directory, list[i]);
	windowTitle = getTitle();
	processFile(mySaveCropFolder, windowTitle, min_area, max_circ);
	close(windowTitle);
}


// DEFINED FUNCTIONS
function OpenFile(dir, fileName) { 
	path=dir+fileName;
	open(path);
	run("Properties...", "unit=pixel pixel_width=1 pixel_height=1 voxel_depth=1.0000000 frame=[1 sec] global");
}

function processFile(dir, title, min_area, max_circ){
	
	run("Duplicate...", "use");
	getHistogram(values, counts, 256);
	min = values[2];
	max = values[5];
	setMinAndMax(min, max);
	call("ij.plugin.frame.ThresholdAdjuster.setMode", "B&W");
	// changed thresh method
	setAutoThreshold("Huang dark no-reset");
	run("Convert to Mask");
	rename("thresholded");
	
	MeasureCentroids(min_area, max_circ);
	ShuffleResultsAddROIs();
	
	//DeleteOverlappingROIs();
	
	selectWindow("Results"); 
	run("Close");
	close("thresholded");
	
	print("I have detected "+roiManager("count")+" unique neurons");
	roiManager("Save", dir+title+"_RoiSet.zip");
	
	ExportCropsFromROIs(dir, title, maxNeurons);
	num_params = 4;
	params_array = newArray(num_params);
	labels_array = newArray(num_params);
	params_array[0] = min;
	labels_array[0] = "contrast min";
	params_array[1] = max;
	labels_array[1] = "contrast max";
	params_array[2] = min_area;
	labels_array[2] = "particle min area";
	params_array[3] = max_circ;
	labels_array[3] = "particle max circularity";
	ExportParamsToDisk(dir, params_array, labels_array);
}

function ExportParamsToDisk(dir, params_array, labels_array) { 
	file_name = "neuron_detection_params.txt";
	f = File.open(dir+file_name);
	
	for (i = 0; i < params_array.length; i++) {
		print(f, labels_array[i]+"\t"+params_array[i] );
	}
	
	File.close(f);
}

function MeasureCentroids(min_area, max_circ) { 
	run("Set Measurements...", "centroid display redirect=None decimal=3");
	run("Analyze Particles...", "size="+min_area+"-Infinity circularity=0.00-"+max_circ+" display");
}

function ShuffleResultsAddROIs() { 
	// create arrays to contain X and Y positions of detected maxima to shuffle
	particle_x = newArray(nResults);
	particle_y = newArray(nResults);
	// we need a reference array to shuffle both X and Y arrays together
	sorting_array = newArray(nResults);
	// fill in the arrays
	for(i=0; i<nResults; i++) {
		// X and Y coordinates are filled
		particle_x[i] = getResult("X", i);
		particle_y[i] = getResult("Y", i);
		// Fill the sorting array with sequential numbers
		sorting_array[i] = i;
	}
	// Shuffle those sequential numbers to put them in random order
	shuffle(sorting_array);
	// Use the sorting positions of the random array to randomly sort together the X and Y arrays
	
	Array.sort(sorting_array, particle_x, particle_y);
	
	for (i = 0; i < particle_x.length; i++) {
		X = particle_x[i];
		Y = particle_y[i];
	    makeRectangle(X-75, Y-75, 150, 150);
		//run ("Add Selection...");
		//run("Show Overlay");
		roiManager("add");
	}
}

function ExportCropsFromROIs(dir, windowTitle, maxNeurons) { 
	numROIs = roiManager("count");
	selectWindow(windowTitle);
	for(i=0; i<numROIs; i++) {
		if(i<maxNeurons){
			roiManager("Select", i);
			run("Duplicate...", "duplicate");
			folderName = replace(windowTitle, ".tif", "");
			myDir = dir+folderName+File.separator;
			File.makeDirectory(myDir);
			saveAs("Tiff", myDir+windowTitle+"_crop"+i);
			close();
			//run("Select None");
		}
	}
	close("Roi Manager");
}

function DeleteOverlappingROIs() { 
// Delete overlapping ROIs from ROI manager
// from here https://forum.image.sc/t/how-can-i-remove-overlapping-rois-from-roi-manager/44888/7
	nROIs = roiManager("Count");
	overlappingROIs = newArray();
	
	for (i = 0; i < roiManager("Count"); i++){ 
		for (j = 0; j < roiManager("Count"); j++){
		   if (i != j){
		   	   	roiManager("Select", newArray(i,j));
		   		roiManager("AND");
		    
				if (selectionType()>-1) {
					overlappingROIs = Array.concat(overlappingROIs,j);
					j=nROIs;
		   		}
		   }
		}
	}

	roiManager("Select", overlappingROIs);
	roiManager("delete");
	run("Select None");
}

function shuffle(array) {
   n = array.length;  // The number of items left to shuffle (loop invariant).
   while (n > 1) {
      k = randomInt(n);     // 0 <= k < n.
      n--;                  // n is now the last pertinent index;
      temp = array[n];  // swap array[n] with array[k] (does nothing if k==n).
      array[n] = array[k];
      array[k] = temp;
   }
}

// returns a random number, 0 <= k < n
function randomInt(n) {
   return n * random();
}