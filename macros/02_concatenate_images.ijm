// This is mandatory to work with Bio-formats
run("Bio-Formats Macro Extensions");

// Open window to choose directory with data split by well
Directory = getDirectory("Choose a Directory with split files");
// Choose destination folder
outputDirectory = getDirectory("Choose a Directory to save the concatenated files");

// Get list of files
fileList = getFileList(Directory);

// Create a new array that will contain all "wells index" (A01/C02 etc..)
wellsArray = newArray();
for (i = 0; i <fileList.length ; i++) {
		fileNameSplit = split(fileList[i], "-"); // split using -
		well = fileNameSplit[5]; // last element contains the well index
		wellsArray = Array.concat(wellsArray, well); // append only the well index
}

// Keep only unique well indeces
wellsUnique = ArrayUnique(wellsArray); 
//Array.show(wellsUnique);

// Loop through all the wells we have
for (i = 0; i <wellsUnique.length ; i++) {
		well = wellsUnique[i];
		// Take full file list and filter only the ones with the well index
		filesToConcat =Array.filter(fileList, well);
		// Sort it to make sure it's concatenated in order
		Array.sort(filesToConcat);
		
		// Loop through all files for given well and concatenate them
	for (j = 0; j <filesToConcat.length ; j++) {
			OpenFile(filesToConcat[j]);
	}
	// Stack all opened images
	run("Images to Stack", "use");
	//run("8-bit");
    //run("Grays");
    
    // Save them as TIFFs into destination folder
	saveAs("TIFF", outputDirectory+"ConcatenatedWell"+well);
	close();
}
print("Process completed!");

// Function to extract unique values from array
// Taken from here https://imagej.nih.gov/ij/macros/Array_Functions.txt
function ArrayUnique(array) {
	array 	= Array.sort(array);
	array 	= Array.concat(array, 999999);
	uniqueA = newArray();
	i = 0;	
   	while (i<(array.length)-1) {
		if (array[i] == array[(i)+1]) {
			//print("found: "+array[i]);			
		} else {
			uniqueA = Array.concat(uniqueA, array[i]);
		}
   		i++;
   	}
	return uniqueA;

/// Requires run("Bio-Formats Macro Extensions");
function OpenFile(fileToProcess){
	path=Directory+fileToProcess;
	print (path);
	Ext.setId(path);
	Ext.getCurrentFile(fileToProcess);
	Ext.getSeriesCount(seriesCount); // this gets the number of series
	// see http://imagej.1557.x6.nabble.com/multiple-series-with-bioformats-importer-td5003491.html
	for (j=0; j<seriesCount; j++) {
        Ext.setSeries(j);
        Ext.getSeriesName(seriesName);
//        print (seriesName);
        Ext.getSizeX(sizeX);
//        print(sizeX);
		// read the series only if it has the desired size (hierarchy of resolution)
        if(sizeX<20000 && sizeX>6000){
        	run("Bio-Formats Importer", "open=&path color_mode=Default view=Hyperstack stack_order=XYCZT series_"+j+1);

        }
	}
}